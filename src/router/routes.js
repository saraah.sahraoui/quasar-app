const routes = [
  {
    path: "/",
    component: () => import("layouts/MainLayout.vue"),
    children: [
      { path: "", component: () => import("pages/Index.vue") },
      {
        path: "/newemployee",
        component: () => import("src/pages/NewEmployee.vue")
      },
      {
        path: "/search",
        component: () => import("src/pages/SearchEmployee.vue")
      },
      {
        path: "/statistics",
        component: () => import("src/pages/Statistics.vue")
      },
      {
        path: "/benefits",
        component: () => import("src/pages/Benefits.vue")
      },
      {
        path: "/timetable",
        component: () => import("src/pages/Timetable.vue")
      },
      {
        path: "/meeting",
        component: () => import("src/pages/Meeting.vue")
      }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: "*",
    component: () => import("pages/Error404.vue")
  }
];

export default routes;
